package com.gitlab.soshibby.issuereporter.logaggregators.configs;

public class LogStatementMapping {
    private String traceId;
    private String spanId;
    private String parentSpanId;
    private String exceptionSpanId;
    private String stacktrace;
    private String date;

    public String getTraceId() {
        return traceId;
    }

    public LogStatementMapping setTraceId(String traceId) {
        this.traceId = traceId;
        return this;
    }

    public String getSpanId() {
        return spanId;
    }

    public void setSpanId(String spanId) {
        this.spanId = spanId;
    }

    public void setParentSpanId(String parentSpanId) {
        this.parentSpanId = parentSpanId;
    }

    public String getParentSpanId() {
        return parentSpanId;
    }

    public String getExceptionSpanId() {
        return exceptionSpanId;
    }

    public void setExceptionSpanId(String exceptionSpanId) {
        this.exceptionSpanId = exceptionSpanId;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    public LogStatementMapping setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
        return this;
    }

    public String getDate() {
        return date;
    }

    public LogStatementMapping setDate(String date) {
        this.date = date;
        return this;
    }
}
