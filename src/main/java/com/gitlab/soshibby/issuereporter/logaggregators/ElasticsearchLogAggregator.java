package com.gitlab.soshibby.issuereporter.logaggregators;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.logaggregator.LogAggregator;
import com.gitlab.soshibby.issuereporter.logaggregators.configs.Config;
import com.gitlab.soshibby.issuereporter.logaggregators.configs.ConfigValidator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.UnknownHostException;
import java.util.List;
import java.util.function.Consumer;

@Component
public class ElasticsearchLogAggregator implements LogAggregator {

    private static final Logger log = LoggerFactory.getLogger(ElasticsearchLogAggregator.class);
    private Thread thread;
    private LogFacade logFacade;
    private Consumer<LogStatement> consumer;
    private Config config;
    private ConfigValidator configValidator;

    public ElasticsearchLogAggregator(ConfigValidator configValidator) {
        this.configValidator = configValidator;
    }

    @Override
    public void init(String configuration) {
        config = Config.from(configuration);
        configValidator.validate(config);

        try {
            logFacade = new LogFacade(config);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void start(Consumer<LogStatement> consumer) {
        log.info("Starting Elasticsearch Log Aggregator.");
        this.consumer = consumer;
        thread = new Thread(this::aggregate);
        thread.start();
    }

    @Override
    public void stop() {
        log.info("Stopping Elasticsearch Log Aggregator.");
        thread.interrupt();
    }

    @Override
    public List<LogStatement> getLogs(String traceId) {
        log.info("Fetching logs for trace id: " + traceId);
        return logFacade.getLogs(traceId);
    }

    private void aggregate() {
        log.info("Starting aggregating logs in 30 seconds.");
        DateTime startDate = DateTime.now();

        while(!thread.isInterrupted()) {
            DateTime endDate = DateTime.now().minusSeconds(30);

            if (startDate.isBefore(endDate)) {
                List<LogStatement> logs = logFacade.getLogs(startDate, endDate);
                logs.forEach(consumer);
                startDate = endDate;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
