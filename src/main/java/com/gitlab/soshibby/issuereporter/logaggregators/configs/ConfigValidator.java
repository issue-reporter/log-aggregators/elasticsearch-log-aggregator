package com.gitlab.soshibby.issuereporter.logaggregators.configs;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class ConfigValidator {
    public void validate(Config config) {
        Assert.notNull(config, "Configuration is null.");
        Assert.hasText(config.getHost(), "Host is null or empty in config.");
        Assert.hasText(config.getIndex(), "Index is null or empty in config.");
        Assert.notNull(config.getPort(), "Port is null in config.");
        Assert.notNull(config.getMappings(), "Mappings is null in config.");
        Assert.hasText(config.getMappings().getTraceId(), "Mappings.traceId is null or empty in config.");
        Assert.hasText(config.getMappings().getSpanId(), "Mappings.spanId is null or empty in config.");
        Assert.hasText(config.getMappings().getDate(), "Mappings.date is null or empty in config.");
        Assert.hasText(config.getMappings().getStacktrace(), "Mappings.stacktrace is null or empty in config.");
    }

}
