package com.gitlab.soshibby.issuereporter.logaggregators.configs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

@JsonIgnoreProperties("class")
public class Config {
    private static ObjectMapper mapper = new ObjectMapper();
    private String host;
    private Integer port;
    private String index;
    private LogStatementMapping mappings;
    private Map<String, Object> filters;

    public String getHost() {
        return host;
    }

    public Config setHost(String host) {
        this.host = host;
        return this;
    }

    public Integer getPort() {
        return port;
    }

    public Config setPort(Integer port) {
        this.port = port;
        return this;
    }

    public String getIndex() {
        return index;
    }

    public Config setIndex(String index) {
        this.index = index;
        return this;
    }

    public LogStatementMapping getMappings() {
        return mappings;
    }

    public Config setMappings(LogStatementMapping mappings) {
        this.mappings = mappings;
        return this;
    }

    public Map<String, Object> getFilters() {
        return filters;
    }

    public Config setFilters(Map<String, Object> filters) {
        this.filters = filters;
        return this;
    }

    public static Config from(String config) {
        try {
            return mapper.readValue(config, Config.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
