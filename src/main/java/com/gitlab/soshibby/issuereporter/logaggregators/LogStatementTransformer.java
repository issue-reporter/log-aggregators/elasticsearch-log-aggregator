package com.gitlab.soshibby.issuereporter.logaggregators;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.logaggregators.configs.LogStatementMapping;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.Map;

public class LogStatementTransformer {
    private LogStatementMapping mapping;

    public LogStatementTransformer(LogStatementMapping mapping) {
        this.mapping = mapping;
    }

    public LogStatement transform(Map<String, Object> fields) {
        try {
            String traceId = asString(mapping.getTraceId(), fields);
            String spanId = asString(mapping.getSpanId(), fields);
            String parentSpanId = asString(mapping.getParentSpanId(), fields);
            String exceptionSpanId = asString(mapping.getExceptionSpanId(), fields);
            String stacktrace = asString(mapping.getStacktrace(), fields);
            Date date = asDate(mapping.getDate(), fields);

            if (traceId == null) {
                throw new RuntimeException("Couldn't find trace id field in '" + fields + "'.");
            }

            if (date == null) {
                throw new RuntimeException("Couldn't find date field in '" + fields + "'.");
            }

            return new LogStatement(traceId, spanId, parentSpanId, exceptionSpanId, stacktrace, fields, date);
        } catch (Exception e) {
            throw new RuntimeException("Failed to transform log statement '" + mapping + "'.");
        }
    }

    private Date asDate(String key, Map<String, Object> fields) {
        if (fields.containsKey(key)) {
            try {
                String value = fields.get(key).toString();
                return DateTime.parse(value).toDate();
            } catch (Exception e) {
                throw new RuntimeException("Failed to parse date from log statement '" + fields.get(key) + "'.");
            }
        }

        return null;
    }

    private String asString(String key, Map<String, Object> fields) {
        if (fields.containsKey(key)) {
            return fields.get(key).toString();
        }

        return null;
    }
}
