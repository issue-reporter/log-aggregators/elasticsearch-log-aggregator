package com.gitlab.soshibby.issuereporter.logaggregators;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.logaggregators.configs.Config;
import com.google.common.collect.Lists;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.joda.time.DateTime;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LogFacade {

    private Config config;
    private LogStatementTransformer transformer;
    private TransportClient client;

    public LogFacade(Config config) throws UnknownHostException {
        this.config = config;
        this.transformer = new LogStatementTransformer(config.getMappings());
        this.client = new PreBuiltTransportClient(Settings.EMPTY)
                .addTransportAddress(new TransportAddress(InetAddress.getByName(config.getHost()), config.getPort()));
    }

    public List<LogStatement> getLogs(String requestId) {
        List<LogStatement> result = new ArrayList<>();

        SearchResponse searchResponse = client.prepareSearch(config.getIndex())
                .setScroll(new TimeValue(60000))
                .setQuery(QueryBuilders.matchPhraseQuery(config.getMappings().getTraceId(), requestId))
                .get();

        do {
            result.addAll(transformToLogStatement(searchResponse));
            searchResponse = client.prepareSearchScroll(searchResponse.getScrollId()).setScroll(new TimeValue(60000)).execute().actionGet();
        } while (searchResponse.getHits().getHits().length != 0);

        return result;
    }

    public List<LogStatement> getLogs(DateTime startDate, DateTime endDate) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();

        query.filter(QueryBuilders.rangeQuery(config.getMappings().getDate())
                .gte(startDate.toString())
                .lt(endDate.toString()));

        if (config.getFilters() != null && !config.getFilters().isEmpty()) {
            config.getFilters().forEach((key, value) -> query.filter(QueryBuilders.matchPhraseQuery(key, value)));
        }

        SearchResponse searchResponse = client.prepareSearch(config.getIndex())
                .setQuery(query)
                .get();

        return transformToLogStatement(searchResponse);
    }

    private List<LogStatement> transformToLogStatement(SearchResponse searchResponse) {
        return Lists.newArrayList(searchResponse.getHits()).stream()
                .map(SearchHit::getSourceAsMap)
                .map(transformer::transform)
                .collect(Collectors.toList());
    }

}
